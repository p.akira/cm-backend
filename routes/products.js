const express = require('express')
const router = express.Router()
const Product = require('../models/product')

//Retrieve all
router.get('/', async (req, res) => {
  try {
    const page = parseInt(req.query.page, 10) || 1
    const size = parseInt(req.query.size) || 16
    const searchText = (req.query.search) || ''

    const products = await Product.paginate({$or: [
        { name: new RegExp(searchText, "gi") },
        { shortDesc: new RegExp(searchText, "gi") },
        ] }, 
      {page: page, limit: size}, 
      function(err, result) {
        if (err) {
          console.error(err)
        }
    })
    res.json(products)
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

//Retrieve a single product
router.get('/:id', getProduct, (req, res) => {
  res.json(res.product)
})

//Create new products
router.post('/', async (req, res) => {
  const product = new Product({
    name: req.body.name,
    shortDesc: req.body.shortDesc,
    price: req.body.price,
    discountPrice: req.body.discountPrice
  })

  try {
    const newProduct = await product.save()
    res.status(201).json(newProduct)
  } catch (err) {
    res.status(400).json({ message: err.message })
  }
})

//Update a product
router.patch('/:id', getProduct, async (req, res) => {
  if (req.body.name != null) {
    res.product.name = req.body.name
  }
  if (req.body.shortDesc != null) {
    res.product.shortDesc = req.body.shortDesc
  }
  if (req.body.price != null) {
    res.product.price = req.body.price
  }
  if (req.body.discountPrice != null) {
    res.product.discountPrice = req.body.discountPrice
  }

  try {
    const updatedProduct = await res.product.save()
    res.json(updatedProduct)
  } catch (err) {
    res.status(400).json({ message: err.message })
  }
})

//Delete a product
router.delete('/:id', getProduct, async (req, res) => {
  try {
    await res.product.remove()
    res.json({ message: 'Product removed from the catalog' })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

async function getProduct(req, res, next) {
  let product
  try {
    product = await Product.findById(req.params.id)
    if (product == null) {
      return res.status(404).json({ message: 'Product not found' })
    }
  } catch (err) {
    return res.status(500).json({ message: err.message })
  }

  res.product = product
  next()
}

module.exports = router