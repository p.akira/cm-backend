const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    shortDesc: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    discountPrice: {
        type: Number,
        required: false
    }
})

productSchema.index({ name: 'text', shortDesc: 'text'}, {weights: {name: 5, body: 3}})
productSchema.plugin(mongoosePaginate)

module.exports = mongoose.model('Product', productSchema)